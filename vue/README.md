# VUE JS.
## SIGUEME EN MIS REDES SOCIALES
- [Visita mi canal de youtube ](https://www.youtube.com/tecnoprofe)
- [Sigueme en Facebook](https://www.facebook.com/zambranachaconjaime/)

# EJERCICIOS
## 000 Vue JS [Código Fuente](hhttps://gitlab.com/tecnoprofe/desarrolloweb/-/blob/master/vue/000%20vue%20v3.html)
***
Utilización de propiedades v-model, eventos y listas de objetos.
***
[![imagen](img/000.jpg)](https://gitlab.com/tecnoprofe/desarrolloweb/-/blob/master/vue/000%20vue%20v3.html)

## 001 Vue JS [Código Fuente](https://gitlab.com/tecnoprofe/desarrolloweb/-/blob/master/vue/002%20vue%20v3-v-bind-y-clases-css.html)
***
Utilización de propiedades v-bind, y manejo de estilos y clases. Ademas de la utilización de los metodos: mouseDown, mouseMove y mouseUp.
***
[![imagen](img/001.jpg)](https://gitlab.com/tecnoprofe/desarrolloweb/-/blob/master/vue/002%20vue%20v3-v-bind-y-clases-css.html)