# TITULO DEL PROYECTO

## Resumen
Breve descripción del proyecto, sus objetivos y resultados esperados.

## Abstract
A brief description of the project in English, its goals, and expected outcomes.

## Demostración del proyecto
![Imagen GIF relacionada con el proyecto](https://media.giphy.com/media/DsadgYrnhOg9O/giphy.gif)
## Integrantes del Equipo de Desarrollo

- [![Facebook](https://img.shields.io/badge/-Facebook-blue?style=flat&logo=Facebook)](URL_del_perfil_de_Facebook_del_Integrante_2) Univ. Xxx YYY ZZZ
- [![Facebook](https://img.shields.io/badge/-Facebook-blue?style=flat&logo=Facebook)](URL_del_perfil_de_Facebook_del_Integrante_3) Univ. Xxx YYY ZZZ

### Docente
- [![Facebook](https://img.shields.io/badge/-Facebook-blue?style=flat&logo=Facebook)](https://www.facebook.com/zambranachaconjaime/) PhD. Jaime Zambrana Chacón
# MARCO TEÓRICO

## Concepción Básica de Informática: Programación Básica en Python
La programación informática es el proceso de diseñar y escribir código fuente para crear programas ejecutables que realizan tareas específicas o resuelven problemas. Python, en particular, se ha destacado como un lenguaje de programación de alto nivel, interpretado y de propósito general. Su diseño enfocado en la legibilidad del código permite a los programadores expresar conceptos en menos líneas de código en comparación con otros lenguajes, como C++ o Java.

Python es conocido por su sintaxis simple y clara, lo que lo hace ideal para principiantes en programación. Admite múltiples paradigmas de programación, incluyendo programación orientada a objetos, imperativa y, en menor medida, funcional.

### Ejemplo de Código Básico en Python

A continuación se muestra un ejemplo básico de código en Python que demuestra una simple operación de suma:

```python
# Ejemplo de Suma en Python

def sumar(a, b):
    return a + b

resultado = sumar(5, 3)
print("El resultado de la suma es:", resultado)

```
# DESARROLLO

Durante la fase de desarrollo, se implementaron varias funcionalidades clave para el sistema. A continuación, se presentan algunas de las características más destacadas con sus respectivas imágenes y descripciones.

![Imagen del Sistema](URL_de_la_imagen_del_sistema_aquí)
*Descripción breve de la imagen.*

![Otra Imagen del Sistema](URL_de_otra_imagen_del_sistema_aquí)
*Descripción breve de la segunda imagen.*

# CONCLUSIONES

Las conclusiones del proyecto son las siguientes:

1. La programación en Python facilita la implementación de sistemas complejos debido a su sintaxis intuitiva y su amplia biblioteca de soporte.
2. El desarrollo iterativo y el testeo continuo contribuyeron significativamente a la eficiencia del proceso de desarrollo.
3. La colaboración y el intercambio de conocimientos dentro del equipo fueron fundamentales para superar los retos técnicos.

# BIBLIOGRAFÍA

- Autor, A. (Año). *Título del libro*. Editorial.
- Autor, B. (Año). "Título del artículo". *Nombre de la revista*, Volumen(Issue), páginas.
- Autor, C. (Año). *Título del libro*. Editorial.

---

Universidad Privada de Santa Cruz de la Sierra (UPDS) - Comprometidos con la excelencia educativa en Santa Cruz, Bolivia.
