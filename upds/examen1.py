contador = 0
while contador < 5:
    print(contador)
    contador += 1

# a) Números del 1 al 5.
# b) Números del 0 al 4.
# c) Números del 0 al 5.
# d) Error.


for numero in range(5):
    print(numero)
# a) Números del 1 al 5.
# b) Números del 0 al 4.
# c) Números del 0 al 5.
# d) Error.