import cv2 
import os
face_cascade=cv2.CascadeClassifier(os.path.dirname(__file__)+'//haarcascade_frontalface_default.xml')
print("archivo:",face_cascade)
cap=cv2.VideoCapture(0)
while True:
    ret, frame=cap.read()
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    if face_cascade.empty():
        raise IOError('No se pudo cargar el archivo XML del calsificador de Haar')
    faces=face_cascade.detectMultiScale(gray, 1.1, 3)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x,y), (y+w, y+h), (0, 255, 0), 2)  
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()